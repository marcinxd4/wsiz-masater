# Runtime
FROM microsoft/dotnet:2.2-aspnetcore-runtime
WORKDIR /publish
COPY /publish .
ENTRYPOINT ["dotnet", "Streetwood.API.dll"]